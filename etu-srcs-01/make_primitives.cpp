#include <iostream>
#include <vector>
#include <iterator>

using namespace std;

void make_prism_shell(int n, vector <vector<double>> polygon, vector <double>  offset,
		      vector <vector<double>> &vertices, vector <vector<int>> &faces){
  
  // Make vertices
  for (auto v : polygon)
  {
    // Generate the extruded vertex
    vector<double> new_vertex(3);
    new_vertex[0] = v[0] + offset[0];
    new_vertex[1] = v[1] + offset[1];
    new_vertex[2] = v[2] + offset[2];

    // Add vertices to the collection
    vertices.push_back(v);
    vertices.push_back(new_vertex);
  }

  for (size_t i = 0; i < vertices.size(); i += 2)
  {
    vector<int> current_face(n);
    int k = 0;

    for (int h = 0; h < n / 2; h++) 
    {
      size_t id = i + h;

      if (id >= vertices.size())
      {
        id -= vertices.size();
      }

      current_face[k] = id + 1;
      k++;
    }

    for (int h = n - 1; h >= n / 2; h--) 
    {
      size_t id = i + h;

      if (id >= vertices.size())
      {
        id -= vertices.size();
      }

      current_face[k] = id + 1;
      k++;
    }

    faces.push_back(current_face);
  }
}

void make_prism(int n, vector <vector<double>> polygon, vector <double>  offset,
		vector <vector<double>> &vertices, vector <vector<int>> &faces){

  // Make vertices
  for (auto v : polygon)
  {
    // Generate the extruded vertex
    vector<double> new_vertex(3);
    new_vertex[0] = v[0] + offset[0];
    new_vertex[1] = v[1] + offset[1];
    new_vertex[2] = v[2] + offset[2];

    // Add vertices to the collection
    vertices.push_back(v);
    vertices.push_back(new_vertex);
  }
  
  vector<int> face_even(n);
  vector<int> face_odd(n);
  int j, k;

  for (size_t i = 0; i < vertices.size(); i++)
  {
    if (i % 2 == 0)
    {
      face_even[j] = i + 1;
      j++;
    }
    else
    {
      face_odd[k] = i + 1;
      k++;
    }
  }

  faces.push_back(face_even);
  faces.push_back(face_odd);

  for (size_t i = 0; i < vertices.size(); i += 2)
  {
    vector<int> current_face(n);
    int k = 0;

    for (int h = 0; h < n / 2; h++) 
    {
      size_t id = i + h;

      if (id >= vertices.size())
      {
        id -= vertices.size();
      }

      current_face[k] = id + 1;
      k++;
    }

    for (int h = n - 1; h >= n / 2; h--) 
    {
      size_t id = i + h;

      if (id >= vertices.size())
      {
        id -= vertices.size();
      }

      current_face[k] = id + 1;
      k++;
    }

    faces.push_back(current_face);
  }
}

void make_pyramid(int n, vector <vector<double>> polygon, vector <double>  appex,
		  vector <vector<double>> &vertices, vector <vector<int>> &faces){

  // Add the top appex vertex
  vertices.push_back(appex);

  // Make vertices and base face
  vector<int> base_face(n);
  int index = 1;
  
  for (auto v : polygon)
  {
    // Add vertices to the collection
    vertices.push_back(v);

    // Add vertices to the base face
    base_face[index - 1] = index + 1;
    index++;
  }

  faces.push_back(base_face);

  // Make other faces
  for(size_t i = 2; i < vertices.size(); i++)
  {
    vector<int> current_face(3);

    current_face[0] = i;
    current_face[1] = i + 1;
    current_face[2] = 1; // for the appex vertex, which is index 0

    faces.push_back(current_face);
  }

  // Add the last face to complete the pyramid
  vector<int> last_face(3);

  last_face[0] = 2; // the first base vertex's index
  last_face[1] = vertices.size(); // the last base vertex's index
  last_face[2] = 1; // for the appex vertex, which is index 0

  faces.push_back(last_face);
}

