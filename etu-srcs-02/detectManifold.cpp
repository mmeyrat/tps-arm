#include "detectManifold.h"

double distance_vertices(vef_vertex v0, vef_vertex v1)
{
    
    // return sqrt( (v2.coordinates[0] - v1.coordinates[0])*(v2.coordinates[0] - v1.coordinates[0])
    //                      + (v2.coordinates[1] - v1.coordinates[1])*(v2.coordinates[1] - v1.coordinates[1]) 
    //                      + (v2.coordinates[2] - v1.coordinates[2])*(v2.coordinates[2] - v1.coordinates[2]) );
}

int detect_no_manifold_vertex(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
	return -1;
}

int detect_no_manifold_edge(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
	return -1;
}

int detect_isolated_vertices(struct vef &vef)
{
    // check edge lenght is not null
    vector<vef_edge> edges = vef.edgesVF;
    for(auto e : edges)
    {
        int v0Index = e.vertices[0];
        int v1Index = e.vertices[1];
        vef_vertex v0 = vef.verticesEF[v0Index];
        vef_vertex v1 = vef.verticesEF[v1Index];

        double lenght = distance_vertices(v0, v1);
        if(lenght == 0.0)
            return 1;
    }

    // check face area is not null
    // TODO 

	return -1;
}

int detect_superposed_vertices(struct vef &vef, double epsilon)
{
    vector<vef_vertex> vertices = vef.verticesEF;
    vector<vef_edge> edges = vef.edgesVF;

    for (int i = 0; i < vertices.size() - 1; i++)
    {
        for (int j = 0; j < vertices.size(); j++)
        {
            if(check_vertex_vertex_intersection(vef, i, j, epsilon) == 1)
                return 1;

            // TODO check the others condition
        }
    }

    return 0;
}

int check_vertex_vertex_intersection(struct vef &vef, int v1Index, int v2Index, double epsilon)
{
    vef_vertex v1 = vef.verticesEF[v1Index];
    vef_vertex v2 = vef.verticesEF[v2Index];
    
    double dist = distance_vertices(v1, v2);
    return dist <= epsilon;
}

int detect_isolated_edges(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
	return -1;
}

int detect_vertex_edge_intersection(struct vef &vef, double epsilon)
{
    vector<vef_vertex> vertices = vef.verticesEF;
    vector<vef_edge> edges = vef.edgesVF;

    // FIXME check if it's better to do for(vertices){for(edges){...}}
    for(auto e : edges)
    {
        for(auto v : vertices)
        {
            vector<int> adjacentEdges = v.edges;
            
            // Check e is not an adjacent edge of v
            if(std::find(adjacentEdges.begin(), adjacentEdges.end(), e.ind) != adjacentEdges.end())
            {
                int v1Index = e.vertices[0];
                int v2Index = e.vertices[1];

                if(check_vertex_vertex_intersection(vef, v.ind, v1Index, epsilon)
                    || check_vertex_vertex_intersection(vef, v.ind, v2Index, epsilon) )
                {
                    return 1;
                }

                // TODO complete
                // if(  ) // v on e
                // {
                //     int vertex
                // }
            }
        }
    }
	return -1;
}

int detect_vertex_face_intersection(struct vef &vef, double epsilon)
{
    vector<vef_vertex> vertices = vef.verticesEF;
    vector<vef_face> faces = vef.facesVE;

    for(auto f : faces)
    {
        for(auto v : vertices)
        {
            vector<int> verticesFromF = f.vertices;
            for(auto w : verticesFromF)
            {
                if(check_vertex_vertex_intersection(vef, v.ind, w, epsilon))
                {
                    return 1;
                }
            }

            vector<int> edgesFromF = f.edges;
            for(auto e : edgesFromF)
            {
                // if(check_vertex_edge_intersection(vef, v.ind, e, epsilon))
                // {
                //     return 1;
                // }
            }

            // if ( v on f )
            // TODO complete
            // ...
        }
    }

    return 0;
}

int detect_isolated_faces(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
	return -1;
}

int detect_edge_on_edge(struct vef &vef)
{
    vector<vef_edge> edges0 = vef.edgesVF;
    vector<vef_edge> edges1 = vef.edgesVF;

    for(auto e0 : edges0)
    {
        for(auto e1 : edges1)
        {
            vector<int> vertices0 = e0.vertices;
            vector<int> vertices1 = e1.vertices;

            if (   ( (vertices0[0] == vertices1[0]) && (vertices0[1] == vertices1[1]) )
                    || ( (vertices0[1] == vertices1[0]) && (vertices0[0] == vertices1[1]) ) )
            {
                return 1;
            }
        }
    }

    return 0;
}

int detect_auto_intersection(struct vef &vef)
{
    double epsilon = 0.01;

    // check if there is a vertex which belongs to an edge that is not a neighbor 
    int checkEdge = detect_vertex_edge_intersection(vef, epsilon);
    if(checkEdge)
        return 1;

    // check if there is a vertex which belongs to a face that is not a neighbor 
    int checkFace = detect_vertex_face_intersection(vef, epsilon);
    if(checkFace)
        return 1;

    // check if an edge cover an other
    // FIXME
    int checkEdgeOnEdge = detect_edge_on_edge(vef);
    if(checkEdgeOnEdge)
        return 1;

    // check if an edge is countained in an other
    // TODO

    // check if an edge intersect a face
    // TODO

    // check if a face is included in an other
    // TODO
    
    return 0;
}

int detect_border_error(struct vef &vef)
{
    vector<vef_edge> edges = vef.edgesVF;

    for(auto e : edges)
    {
        vector <pair<int,int>> faces = e.faces; 
        if(faces.size() != 2)
            return 1;
        
        // TODO check if faces' orientation are opposed
    }

	return -1;
}

int detect_closed_volume_error(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
	return -1;
}

bool checkNMVertex(struct vef &v_e_f, int v)
{
	int v_faces_quantity = v_e_f.verticesEF[v].faces.size();
	bool f_use[v_faces_quantity]; // false : UNUSED ; true : USED 

	for (int i = 0; i < v_faces_quantity; i++)
	{
		f_use[i] = false;
	}

	int f = v_e_f.verticesEF[v].faces[0];

	if (f != NULL) 
	{
		while (f != NULL)
		{
			f_use[f] = true;
			int f_next = NULL;

			vector <int> faces_adj;
			vector <int> edges = v_e_f.facesVE[f].edges;
			
			for (int i = 0; i < edges.size(); i++)
			{
				if (v_e_f.edgesVF[edges[i]].faces[0].first == f)
				{
					faces_adj.push_back(v_e_f.edgesVF[edges[i]].faces[1].first);
				} 
				else
				{
					faces_adj.push_back(v_e_f.edgesVF[edges[i]].faces[0].first);
				}
			}

			for (int i = 0; i < faces_adj.size(); i++)
			{
				for (int k = 0; k < v_faces_quantity; k++)
				{
					if (v_e_f.verticesEF[v].faces[k] == faces_adj[i] && !f_use[k])
					{
						f_next = faces_adj[i];
					}
				}
			}

			f = f_next;
		}
	}

	for (int i = 0; i < v_faces_quantity; i++) 
	{
		if (!f_use[i])
		{
			return true;
		}
	}

	return false;
}

int checkNMEdge(struct vef &v_e_f, int e)
{
	return v_e_f.edgesVF[e].faces.size() > 2;
}
