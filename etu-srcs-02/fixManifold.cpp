#include "fixManifold.h"

int fix_no_manifold_vertex(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_no_manifold_edge(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_isolated_vertices(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_isolated_edgess(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_isolated_faces(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_auto_intersection(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_covering(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_border_error(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}

int fix_closed_volume_error(vector <vector<double>> &vertices, vector <vector<int>> &faces)
{
    return NULL;
}
