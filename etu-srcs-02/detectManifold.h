#ifndef DETECT_MANIFOLD_H
#define DETECT_MANIFOLD_H

using namespace std;

#include <vector>
#include <math.h>
#include <vector>
#include <algorithm>

int detect_no_manifold_vertex(vector <vector<double>> &vertices, vector <vector<int>> &faces);
int detect_no_manifold_edge(vector <vector<double>> &vertices, vector <vector<int>> &faces);

int detect_isolated_vertices(vector <vector<double>> &vertices, vector <vector<int>> &faces);
int detect_isolated_edges(vector <vector<double>> &vertices, vector <vector<int>> &faces);
int detect_isolated_faces(vector <vector<double>> &vertices, vector <vector<int>> &faces);

int detect_superposed_vertices(struct vef &vef, double epsilon);

int detect_auto_intersection(struct vef &vef);

int detect_border_error(struct vef &vef);
int detect_closed_volume_error(vector <vector<double>> &vertices, vector <vector<int>> &faces);

int detect_vertex_edge_intersection(struct vef &vef, double epsilon);
int detect_vertex_face_intersection(struct vef &vef, double epsilon);

int check_vertex_vertex_intersection(struct vef &vef, int v1Index, int v2Index, double epsilon);

bool checkNMVertex(struct vef &v_e_f, int v);
int checkNMEdge(struct vef &v_e_f, int e);

#endif /*  DETECT_MANIFOLD_H */
