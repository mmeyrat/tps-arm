// #include <vector>
// #include <iostream>
// #include <fstream>


// #include "vef.h"
// using namespace std;

// tuple <int, int, int> vef_from_wavefront(struct vef &v_e_f, const char* file_name)
// {
//   cout << file_name << " read \n";

//   std::ifstream file_in;
//   std::string _token;

//   int vertex_ind = 1;
//   int edge_ind = 1;
//   int face_ind = 1;

//   file_in.open (file_name);

//   if (!file_in.good())
//   {
//     file_in.clear();
//     cout<< ": file_in problems \n";
//     exit(1);
//   }
  
//   // Initialize vef struct
//   vector<vef_vertex> vertices(0);
//   vector<vef_edge> edges(0);
//   vector<vef_face> faces(0);

//   // Read and parse the file
//   file_in.seekg(0);
  
//   while(!file_in.eof())
//   {
//     _token = ""; // Reset token.
//     file_in >> _token;
    
//     // Handle VEF vertices
//     if(_token == "v")
//     {
//       // Get current vertex coordinates
//       vector<double> vertex(3);
//       file_in >> vertex[0] >> vertex[1] >> vertex[2];

//       // Create the corresponding vef_vertex
//       vef_vertex current_vertex;
//       current_vertex.ind = vertex_ind;
//       current_vertex.coordinates.push_back(vertex[0]);
//       current_vertex.coordinates.push_back(vertex[1]);
//       current_vertex.coordinates.push_back(vertex[2]);
//       vertex_ind++;

//       // Add the vef_vertex to v_e_f struct
//       v_e_f.verticesEF.push_back(current_vertex);
//     }
    
//     // Handle OBJ faces
//     if(_token == "f")
//     {
//       // Get the vertices of the current face
//       std::string tmp = "";
//       getline(file_in, tmp);
//       std::istringstream line(tmp);
//       std::string tmp2 = "";

//       // Create the corresponding vef_face
//       vef_face current_face;
//       current_face.ind = face_ind;
//       while(line >> tmp2) {
//         int vertex_index = atoi(tmp2.c_str());
//         current_face.vertices.push_back(vertex_index);

//         // Add this face in current vertex list
//         v_e_f.verticesEF[vertex_index - 1].faces.push_back(face_ind);
//       }

//       face_ind++;

//       // Add the vef_face to v_e_f struct
//       v_e_f.facesVE.push_back(current_face); // TODO check the VE specificity
//     }
//   }

//   for (vef_face& f : v_e_f.facesVE)
//   {
//     for (size_t i = 0; i < f.vertices.size(); i++)
//     {
//       vef_edge current_edge;
//       int id = edge_ind;
//       int swap_value = 1;

//       current_edge.ind = edge_ind;

//       int first_vertex = f.vertices[i];
//       int second_vertex = f.vertices[i + 1];

//       if (i + 1 >= f.vertices.size()) // when a face has the final and first vertices
//         second_vertex = f.vertices[0];

//       if (second_vertex < first_vertex) // order the vertices 
//       {
//         swap(first_vertex, second_vertex);
//         swap_value = -1; // store the order
//       }

//       for (vef_edge& e : v_e_f.edgesVF)
//       {
//         if (e.vertices[0] == first_vertex && e.vertices[1] == second_vertex) // check if the edge doesn't exist
//         {
//           id = e.ind;
//           e.faces.push_back(make_pair(f.ind, swap_value)); // add missing face in edge (not added when the edge was created) 
//         }
//       }

//       if (id == edge_ind) // check if id is changed (= the edge already exist)
//       {
//         current_edge.vertices.push_back(first_vertex);
//         current_edge.vertices.push_back(second_vertex);

//         current_edge.faces.push_back(make_pair(f.ind, swap_value));

//         v_e_f.edgesVF.push_back(current_edge);
//         edge_ind++;
//       }

//       f.edges.push_back(id);
//     }
//   }

//   for (vef_vertex& v : v_e_f.verticesEF)
//   {
//     for (vef_edge e : v_e_f.edgesVF)
//     {
//       bool is_present = false;

//       for (size_t i = 0; i < e.vertices.size(); i++)
//       {
//         if (e.vertices[i] == v.ind) // check if edge has not already been added to the current vertex
//           is_present = true;
//       }

//       if (is_present)
//         v.edges.push_back(e.ind);
//     }
//   }

//   return make_tuple(vertex_ind, edge_ind, face_ind);
// }

// int
// vef_to_vef(struct vef &v_e_f, const char* file_name)
// {
//   ofstream file;
//   file.open(file_name);
  
//   file << "# " << file_name << "\n";

//   // Vertices
//   for (vef_vertex v : v_e_f.verticesEF)
//   {
//     file << "v" << v.ind << " < ";

//     for (size_t i = 0; i < v.coordinates.size(); i++)
//     {
//       file << v.coordinates[i] << " ";
//     }

//     file << "> < ";

//     for (size_t i = 0; i < v.edges.size(); i++)
//     {
//       file << "e" << v.edges[i] << " ";
//     }

//     file << "> < ";

//     for (size_t i = 0; i < v.faces.size(); i++)
//     {
//       file << "f" << v.faces[i] << " ";
//     }
    
//     file << ">\n";
//   }
  
//   while (v_e_f.verticesEF.size() > 0) // Clearing all vertices
//   {
//     v_e_f.verticesEF.erase(v_e_f.verticesEF.begin());
//   }

//   file << "\n";

//   // Edges
//   for (vef_edge e : v_e_f.edgesVF)
//   {
//     file << "e" << e.ind << " < ";

//     for (size_t i = 0; i < e.vertices.size(); i++)
//     {
//       file << "v" << e.vertices[i] << " ";
//     }

//     file << "> < ";

//     for (size_t i = 0; i < e.faces.size(); i++)
//     {
//       file << "(f" << e.faces[i].first << "," << e.faces[i].second << ") ";
//     }
    
//     file << ">\n";
//   }
  
//   while (v_e_f.edgesVF.size() > 0) // Clearing all edges
//   {
//     v_e_f.edgesVF.erase(v_e_f.edgesVF.begin());
//   }

//   file << "\n";

//   // Faces
//   for (vef_face f : v_e_f.facesVE)
//   {
//     file << "f" << f.ind << " < ";

//     for (size_t i = 0; i < f.vertices.size(); i++)
//     {
//       file << "v" << f.vertices[i] << " ";
//     }

//     file << "> < ";

//     for (size_t i = 0; i < f.edges.size(); i++)
//     {
//       file << "e" << f.edges[i] << " ";
//     }
    
//     file << ">\n";
//   }

//   while (v_e_f.facesVE.size() > 0) // Clearing all faces
//   {
//     v_e_f.facesVE.erase(v_e_f.facesVE.begin());
//   }

//   printf("ok\n");

//   file.close();
//   return 0;
// }
